#!/bin/bash

echo "Enter name of the process:"
	read proc

echo "Enter date in \"Mon dd\" format:"
	read d

echo "Enter time."
echo "Hour since:"
	read hourSince
echo "Hour to:"
	read hourTo

log="/var/log/"$proc

if ! [ -f $log ];
then log="/var/log/syslog"
fi

let "hourSinceFirst=$hourSince/10"
let "hourSinceSecond=$hourSince%10"

let "hourToFirst=$hourTo/10"
let "hourToSecond=$hourTo%10"

if [ $hourSinceFirst -eq $hourToFirst ]
then grep "$d $hourSinceFirst[$hourSinceSecond-$hourToSecond]:[0-9][0-9]:[0-9][0-9]" $log
else 
	grep "$d $hourSinceFirst[$hourSinceSecond-9]:[0-9][0-9]:[0-9][0-9]" $log
	grep "$d $hourSinceTo[0-$hourToSecond]:[0-9][0-9]:[0-9][0-9]" $log
fi

