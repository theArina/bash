#!/bin/bash

echo "What do you wish?"
echo "1 - set new alarm"
echo "2 - edit existing alarm"
echo "3 - delete alarm"
echo "4 - show all alarms"
echo "0 - exit"

d=''
hour=''
declare -i min=''

function setAlarm {
	
	echo -n "Which day do you want to set the alarm? (yyyy-mm-dd)"
	read d

	echo -n "In what hour do you want to wake up? (hh)"
	read hour

	echo -n "In what minute? (mm)"
	read min

	declare -i z=0
	z=$min+1
	echo "$d $hour:$min"
	#echo "$z"

	#sleep 10
	u=$(date +%s -d "$d $hour:$min")
	#echo "$u"
	
	echo "$min $hour * * * ~/BashScripts/Alarm/alarm.sh" >> ~/BashScripts/cron.txt 
}

function changeAlarm {

	crontab -l 

	echo -n "Which alarm do you want to edit? (hh)"
	read hour

	echo -n "(mm)"
	read min

	sed -i "/$min $hour * * */d" ~/BashScripts/cron.txt

	echo -n "In what hour do you want to set the alarm? (hh)"
	read hour

	echo -n "In what minute? (mm)"
	read min

	echo "$min $hour * * * ~/BashScripts/Alarm/alarm.sh" >> ~/BashScripts/cron.txt 
}

function delAlarm {

	echo -n "Which alarm do you want to delete? (hh)"
	read hour

	echo -n "(mm)"
	read min

	sed -i "/$min $hour * * */d" ~/BashScripts/cron.txt
}

menuNum=''
read menuNum

case "$menuNum" in
1) setAlarm ;;
2) changeAlarm ;;
3) delAlarm ;;
4) crontab -l ;;
0) exit 0 ;;
*) echo "Wrong command, try again" ~/BashScripts/Alarm/./wake.sh ;;
esac

crontab < ~/BashScripts/cron.txt

contin=0
echo "Do you want to continue?[1 -yes, 0 - no]"
read contin
if [[ contin -eq 1 ]]
then ~/BashScripts/Alarm/./wake.sh 
else exit 0
fi 

#crontab -l | grep -v "$min $hour * * * /home/arina/Alarm/alarm.sh"  | crontab

#sed '$min $hour * * * /home/arina/Alarm/alarm.sh/d'
