#!/bin/bash

function userF {
	echo "Enter wishful process:"
	read proc
	echo "Enter wishful time to check (min):"
	read min
	
	echo "*/$min * * * * ~/BashScripts/Processes/Script.sh $proc" >> ~/BashScripts/cron.txt 
	crontab < ~/BashScripts/cron.txt
}

function cronF() {

	[ "$(pidof $1)" ] && echo "$1 is running" >> ../t.txt || $1
	
}

if [ -z $1 ]
then userF
else cronF $1
fi
