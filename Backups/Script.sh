#!/bin/bash

if [ -z $1 ]
then 

	echo "What do you want to backup?"
	read what
	echo "Where to?"
	read whereTo
	echo "How often? \"minute hour day_of_mounth month day_of_week\" (e.c. */3 - once per 3 ...)"
	read frequency
	
	isArchive='2'
	if ! [ -f $what ]
	then 
		echo "Do you want to archive directory?[0-no/1-yes]"
		read isArchive
	fi

	echo "$frequency ~/BashScripts/Backups/Script.sh $what $whereTo $isArchive" >> ~/BashScripts/cron.txt 
		crontab < ~/BashScripts/cron.txt

else 

	if ! [ $3 -eq 2 ]
	then	
		if [ $3 -eq 1 ]
		then 
			tar -cf $2/t.tar $1
			#tar -cf $2/t.tar $1
		else
			rsync -r $1 $2
		fi
	else 
		if [ -b $1 ]
		then
			dd if=$1 of=$2.iso
		else
			cp $1 $2
		fi
	fi

fi
